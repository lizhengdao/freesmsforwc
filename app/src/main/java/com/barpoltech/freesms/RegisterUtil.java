package com.barpoltech.freesms;


import android.content.Context;
import android.graphics.Color;
import android.os.Looper;

import com.loopj.android.http.*;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import cz.msebera.android.httpclient.Header;

public class RegisterUtil {
    static final String cloudFunctionURL = "https://us-central1-sms-sender-e5fb3.cloudfunctions.net/";
    Context context;
    SweetAlertDialog loadingDialog;

    public RegisterUtil(Context context) {
        this.context = context;
    }

    public void registerTokens(MyPreferences prefs) {
        final String qrToken = prefs.get("qrToken");
        final String firebaseToken = prefs.get("fcmToken");

        if (qrToken == null || qrToken.isEmpty() || firebaseToken == null || firebaseToken.isEmpty()) {
            showErrorDialog(context);
            return;
        }

        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                Looper.prepare();
                try  {
                    sendRequest(
                            cloudFunctionURL + "/registerTokens",
                            new HashMap<String, Object>() {{
                                put("qrToken", qrToken);
                                put("firebaseToken", firebaseToken);
                            }});
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Looper.loop();
            }
        });

        thread.start();
    }

    private void sendRequest(String endpoint, Map<String, Object> values) {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams p = new RequestParams();
        for(String key : values.keySet()) {
            p.put(key, values.get(key));
        }

        client.post (endpoint, p, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                showLoading(context);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                loadingDialog.hide();
                showSuccessDialog(context);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                loadingDialog.hide();
                showErrorDialog(context);
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

    private void showSuccessDialog(Context c) {
        SweetAlertDialog pDialog = new SweetAlertDialog(c, SweetAlertDialog.SUCCESS_TYPE);
        pDialog.setTitleText("You're all set!")
                .setContentText("You can close the app now. SMS messages will be sent directly from your phone. You will see them in the app.")
                .show();
    }

    private void showErrorDialog(Context c) {
        SweetAlertDialog pDialog = new SweetAlertDialog(c, SweetAlertDialog.ERROR_TYPE);
        pDialog.setTitleText("Whoops!")
                .setContentText("Make sure you have scanned the correct QR code (the one on Free SMS for WooCommerce config page), and you're connected to the internet. Eventually try to reinstall the app.")
                .show();
    }

    public void showLoading(Context c) {
        loadingDialog = new SweetAlertDialog(c, SweetAlertDialog.PROGRESS_TYPE);
        loadingDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        loadingDialog.setTitleText("Loading");
        loadingDialog.setCancelable(false);
        loadingDialog.show();
    }

}
