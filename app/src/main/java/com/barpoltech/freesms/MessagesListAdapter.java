package com.barpoltech.freesms;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

public class MessagesListAdapter extends ArrayAdapter<Message> {

    private final Context context;
    private final List<Message> messages;

    public MessagesListAdapter(Context context, List<Message> messages) {
        super(context, -1, messages);
        this.context = context;
        this.messages = messages;
    }

    @Override
    public View getView(int index, View convertView, ViewGroup parent) {
        Message msg = messages.get(index);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.list_message, parent, false);
        TextView msgListItemPhone = (TextView) v.findViewById(R.id.msgListItemPhone);
        TextView msgListItemText = (TextView) v.findViewById(R.id.msgListItemText);
        TextView msgListItemDate = (TextView) v.findViewById(R.id.msgListItemDate);
        msgListItemPhone.setText(msg.getPhone());
        msgListItemText.setText(msg.getMessage());
        msgListItemDate.setText(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(msg.getSent()));
        return v;
    }
}
