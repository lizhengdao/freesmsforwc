package com.barpoltech.freesms;
import org.litepal.annotation.Column;
import org.litepal.crud.LitePalSupport;

import java.util.Date;

public class Message extends LitePalSupport {
    @Column()
    private int id;

    @Column()
    private String message;

    @Column()
    private String phone;

    @Column()
    private Date sent;

    @Column()
    private String error;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getSent() {
        return sent;
    }

    public void setSent(Date sent) {
        this.sent = sent;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }


    @Override
    public String toString() {
        return "Message{" +
                "id='" + id + '\'' +
                "message='" + message + '\'' +
                ", phone='" + phone + '\'' +
                ", sent=" + sent +
                ", error='" + error + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
