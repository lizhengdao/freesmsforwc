package com.barpoltech.freesms;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "### MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage.getData().size() > 0) {
            Map<String, String> remoteMsg = remoteMessage.getData();

            SMSSender.send(remoteMsg.get("message"), remoteMsg.get("phone") );
            MessageDAL.addMessage(remoteMsg.get("message"), remoteMsg.get("phone"));
        }
    }

    @Override
    public void onNewToken(String token) {
        new MyPreferences(this).set("fcmToken", token);
    }

}
