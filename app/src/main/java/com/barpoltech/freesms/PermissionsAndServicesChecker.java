package com.barpoltech.freesms;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

public class PermissionsAndServicesChecker {

    Activity activity;
    public PermissionsAndServicesChecker(Activity a) {
        activity = a;
    }

    public boolean GooglePlayCheck () {
        GoogleApiAvailability gApi = GoogleApiAvailability.getInstance();
        int resultCode = gApi.isGooglePlayServicesAvailable (activity);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (gApi.isUserResolvableError(resultCode)) {
                gApi.getErrorDialog(activity, resultCode, 9000).show();
            } else {
                Toast.makeText(activity, "You need to install Google Play Services to use this app.", Toast.LENGTH_LONG).show();
            }
            return false;
        }
        return true;
    }

    public void PermissionsCheck() {
        // ask for camera and sms permissions
        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA, Manifest.permission.SEND_SMS}, 1);
    }
}
