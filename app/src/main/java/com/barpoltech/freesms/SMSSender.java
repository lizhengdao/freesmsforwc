package com.barpoltech.freesms;

import android.telephony.SmsManager;

import java.util.ArrayList;

public class SMSSender {

    public static void send(String message, String number) {
        SmsManager smsManager = SmsManager.getDefault();
        ArrayList<String> msgArray = smsManager.divideMessage(message);
        smsManager.sendMultipartTextMessage(number, null, msgArray, null, null);
    }
}
